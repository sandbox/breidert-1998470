/**
 * @file
 * jQuery plugins to insert text into textareas from a combobox.
 */

// General Insert API functions.
(function ($) {
  $.fn.insertAtCursor = function (tagName) {
    return this.each(function(){
      if (document.selection) {
        //IE support
        this.focus();
        sel = document.selection.createRange();
        sel.text = tagName;
        this.focus();
      }else if (this.selectionStart || this.selectionStart == '0') {
        //MOZILLA/NETSCAPE support
        startPos = this.selectionStart;
        endPos = this.selectionEnd;
        scrollTop = this.scrollTop;
        this.value = this.value.substring(0, startPos) + tagName + this.value.substring(endPos,this.value.length);
        this.focus();
        this.selectionStart = startPos + tagName.length;
        this.selectionEnd = startPos + tagName.length;
        this.scrollTop = scrollTop;
      } else {
        this.value += tagName;
        this.focus();
      }
    });
  };
})(jQuery);

/**
 * Behavior to add "Insert" buttons.
 */
(function ($) {
Drupal.behaviors.TextInsertText = {
  attach: function(context) {
    // Add the click handler to the insert button.
    if(!(typeof(Drupal.settings.text_insert) == 'undefined')){
      for(var key in Drupal.settings.text_insert.buttons){
        $("#" + key, context).click(insert);
      }
    }

    function insert() {
      var field = $(this).attr('id');
      var selectbox = field.replace('button', 'select');
      var content = $('#' + selectbox ).val();
      $('#' + Drupal.settings.text_insert.buttons[field]).insertAtCursor(content);
      return false;
    }
  }
};

})(jQuery);