<?php

/**
 * @file
 * Include to load text snippets from views.
 */
function text_insert_get_texts($view_name, $view_display_id) {

  global $_text_insert_begin_text;
  global $_text_insert_end_text;
  global $_text_insert_begin_label;
  global $_text_insert_end_label;

  $text_insert_options = array();
  $text_insert_default_options = array('demo text 1' => 'demo text 1', 'demo text 2' => 'demo text 2');

  //$view_name = variable_get('text_insert_view_name', "");
  //$view_display_id = variable_get('text_insert_view_display_id', "default");
  //Render the view
  $rendered_view = views_embed_view($view_name, $view_display_id);
  if (!$rendered_view) {
    watchdog("text_insert", "An error occured when rendering view '$view_name' and display '$view_display_id' with function views_embed_view($view_name, $view_display_id) in file text_insert_inc .", null, WATCHDOG_WARNING);
    drupal_set_message(check_plain(t("An error occured when rendering view '$view_name' and display '$view_display_id'. More information in watchdog.")));

    return $text_insert_default_options;
  }

  //Find the text snippets
  $offset = 0;
  while ($begin = strpos($rendered_view, $_text_insert_begin_text, $offset)) {
    $begin += strlen($_text_insert_begin_text);
    $end = strpos($rendered_view, $_text_insert_end_text, $begin);
    if ($end) {
      $length = $end - $begin;
      $snippet = substr($rendered_view, $begin, $length);
      //drupal_set_message("Found snippet '$snippet' in $rendered_view");
      //Find label that is before this snippet, but after the last snippet
      $label = strip_tags($snippet);
      $search_range_label = substr($rendered_view, $offset, $begin);
      $label_begin = strpos($search_range_label, $_text_insert_begin_label);
      if ($label_begin) {
        $label_begin += strlen($_text_insert_begin_label);
        $label_end = strpos($search_range_label, $_text_insert_end_label, $label_begin);
        if ($label_end) {
          $label_length = $label_end - $label_begin;
          $label = strip_tags(substr($search_range_label, $label_begin, $label_length));
          drupal_set_message("Found label $label");
        }
        else {
          drupal_set_message("User standard label");
        }
      }
      //Shorten label
      if (strlen($label) > 63) {
        $label = substr($label, 0, 60) . "...";
      }
      //drupal_set_message("Found snippet: ".htmlentities($snippet));
      $text_insert_options[$snippet] = $label;
      $offset = $end + strlen($_text_insert_end_text);
    }
  }

  //Return default text snippets if no snippets are found.
  if (empty($text_insert_options)) {
    drupal_set_message(check_plain(t("No text snippetw found in view '$view_name' and display '$view_display_id'. More information on the <a href='/admin/help#text_insert'>help page</a>.")));
    return $text_insert_default_options;
  }

  return $text_insert_options;
}

