(function ($) {

$(document).ready(function() {
  //Set default visibility
  if ($('#edit-use-for-text-insert').is(':checked')) {
    $(".form-item-text-insert-view-name").show();
    $(".form-item-text-insert-view-display-id").show();
  } else {
    $(".form-item-text-insert-view-name").hide();
    $(".form-item-text-insert-view-display-id").hide();
  } 
  //Toggle visibility
  $('#edit-use-for-text-insert').click(function() {
    $(".form-item-text-insert-view-name").toggle();
    $(".form-item-text-insert-view-display-id").toggle();
  })
});

})(jQuery);